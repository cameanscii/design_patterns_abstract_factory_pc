package pl.sda.abstractfactorypc;

public class Factory {

        public static AsusPc createAsusPc1(){



            return new AsusPc("AsusPc1" , COMPUTER_BRAND.ASUS, 12, 12.3, true);
        }

        public static AppleMac createAppleMac1(){

            return new AppleMac("AppleMac1" , COMPUTER_BRAND.MAC, 12, 12.3, true);
        }



}
