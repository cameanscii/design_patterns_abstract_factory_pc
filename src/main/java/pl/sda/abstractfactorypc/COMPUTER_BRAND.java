package pl.sda.abstractfactorypc;

public enum COMPUTER_BRAND {
    HP,
    ASUS,
    SAMSUNG,
    MAC;
}
