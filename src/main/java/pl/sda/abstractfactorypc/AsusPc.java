package pl.sda.abstractfactorypc;

public class AsusPc extends AbstractPC {
    public AsusPc(String name, COMPUTER_BRAND computer_brand, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, computer_brand, cpu_power, gpu_power, isOverlocked);
    }

    public static AsusPc createAsusPc1(){

        return new AsusPc("AsusPc1" , COMPUTER_BRAND.ASUS, 12, 12.3, true){};
    }
    public static AsusPc createAsusPc2(){

        return new AsusPc("AsusPc2" , COMPUTER_BRAND.ASUS, 17, 19.3, false){};
    }
}
