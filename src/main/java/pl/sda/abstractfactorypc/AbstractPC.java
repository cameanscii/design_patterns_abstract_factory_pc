package pl.sda.abstractfactorypc;

public abstract class AbstractPC {

    private String name;
    private COMPUTER_BRAND computer_brand;
    private int cpu_power;
    private double gpu_power;
    private boolean isOverlocked;


    public AbstractPC(){}

    public AbstractPC(String name, COMPUTER_BRAND computer_brand, int cpu_power, double gpu_power, boolean isOverlocked) {
        this.name = name;
        this.computer_brand = computer_brand;
        this.cpu_power = cpu_power;
        this.gpu_power = gpu_power;
        this.isOverlocked = isOverlocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public COMPUTER_BRAND getComputer_brand() {
        return computer_brand;
    }

    public void setComputer_brand(COMPUTER_BRAND computer_brand) {
        this.computer_brand = computer_brand;
    }

    public int getCpu_power() {
        return cpu_power;
    }

    public void setCpu_power(int cpu_power) {
        this.cpu_power = cpu_power;
    }

    public double getGpu_power() {
        return gpu_power;
    }

    public void setGpu_power(double gpu_power) {
        this.gpu_power = gpu_power;
    }

    public boolean isOverlocked() {
        return isOverlocked;
    }

    public void setOverlocked(boolean overlocked) {
        isOverlocked = overlocked;
    }

    @Override
    public String toString() {
        return "AbstractPC{" +
                "name='" + name + '\'' +
                ", computer_brand=" + computer_brand +
                ", cpu_power=" + cpu_power +
                ", gpu_power=" + gpu_power +
                ", isOverlocked=" + isOverlocked +
                '}';
    }
}
