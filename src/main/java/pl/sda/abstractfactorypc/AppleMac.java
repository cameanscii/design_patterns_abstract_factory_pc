package pl.sda.abstractfactorypc;

public class AppleMac extends AbstractPC {
    public AppleMac(String name, COMPUTER_BRAND computer_brand, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, computer_brand, cpu_power, gpu_power, isOverlocked);
    }

    public static AppleMac createAppleMac1(){

        return new AppleMac("AppleMac1" , COMPUTER_BRAND.MAC, 12, 12.3, true);
    }
    public static AppleMac createAppleMac2(){

        return new AppleMac("AppleMac2" , COMPUTER_BRAND.MAC, 17, 19.3, false);
    }
}
