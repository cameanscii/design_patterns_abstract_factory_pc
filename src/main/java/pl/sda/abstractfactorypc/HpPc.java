package pl.sda.abstractfactorypc;

public class HpPc extends AbstractPC {
    private HpPc(String name, COMPUTER_BRAND computer_brand, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, computer_brand, cpu_power, gpu_power, isOverlocked);
    }

    public static HpPc createHP1(){
        return new HpPc("CompHp1" , COMPUTER_BRAND.HP, 12, 12.3, true);
    }
    public static HpPc createHP2(){
        return new HpPc("CompHp2" , COMPUTER_BRAND.HP, 17, 19.3, false);
    }
}
