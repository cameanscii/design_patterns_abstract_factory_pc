package pl.sda.main;

import pl.sda.abstractfactorypc.Factory;
import pl.sda.abstractfactorypc.HpPc;

public class Main {

    public static void main(String[] args) {

        System.out.println(Factory.createAppleMac1());
        System.out.println(HpPc.createHP1());
    }
}
